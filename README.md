Introduction	
The following is a proposal for a graphical movie information application. The application will accept a movie title as input and retrieve its information from TheMovieDB.org.

Model	
The model will accept a string as user input, validate that input and pass it to TheMovieDB.org as a search string. If one or more relevant movie titles are located, the model will parse the first relevant result for the title, release date, summary and movie poster. 

View	
If a valid result has been returned by TheMovieDB.org and parsed by the model, the view will display relevant information.  

If an invalid search string is supplied or no results are returned by TheMovieDB.org an error message will be displayed. 

Controller	
The controller has one method called handleButtonAction() that will be activated when the “Search” button in the view is clicked or the enter/ return keyboard key is pressed. The Model will prompt the user with “Enter a movie title”.

JUnit Tests		
The following JUnit tests, found in MvModelTest.java will be used to validate the Model in MvModel. 

testGetMv1: 	
	use 111122 as an invalid query 
	validate getResult() returns 0

testGetMv2: 	
	use Better Luck Tomorrow as a valid query for getResult
	validate getResult() returns 1

testRsltCnt1: 	
	use "Better Luck Tomorrow" as a valid query
	validate rsltCnt() returns 1

testRsltCnt2: 	
	use "Big Bang Theory" as an invalid query
	validate rsltCnt() returns 0

testRsltCnt3: 	
	use "Better+Luck+Tomorrow" as a valid query
	validate rsltCnt() returns 1

testIsString1: 	
	use " " as an invalid query
	validate isString() returns false

testIsString2: 	
	use "!@#$%^&*()_+=~`- " as an invalid query
	validate isString() returns false

testIsString3: 	
	use "bEtter luck !@#$%^&*()_+= tomorrow" as a valid query
	validate isString() returns true

testIsString4: 	
	use "bEtter luck !@#$%^&*()_+=" as a valid query
	validate isString() returns true



testIsNull1:	
	use “test1” as an invalid query
	validate isNull() returns true

testIsNull2:	
	use “Wall-E” as a valid query
	validate isNull() returns false

testConstrQry1:		
	use “Wall-E” as a valid input
	validate constrQry() returns Wall-E

testConstrQry2:		
	use “ ” (blank space) as an invalid input
	validate constrQry() returns null

testConstrQry3:		
	use “~!@#$%^&**()_” as an invalid input
	validate constrQry() returns null

testConstrQry4:		
	use “John Wick” as a valid input
	validate constrQry() returns “John+Wick”

testConstrQry5:		
	use “John          Wick” (multiple sequential whitespaces) as a valid input
	validate constrQry() returns “John+Wick”

testConstrQry6:		
use “Better     Luck *&%@tomorrow” (multiple sequential whitespaces and accidentals) as a valid input
	validate constrQry() returns “Better+Luck+*&%@tomorrow”

testGetTitle1:		
	use "Better     Luck *&%@tomorrow" as valid input
	validate getTitle() returns "Better Luck Tomorrow"

Files		
The following files are used to create this application: 	
	MvView.fxml – View for project
	MvController.java – Controller for project
	MvModel.java – Model for project
	MvMain.java – Contains public static void main to start the application
	MvModelTest.java – JUnit tests for MvModel.java
gson-2.2.4.jar - Required to parse returned JSON code	
error.jar – Contains error.png image for error display	
	Hamcrest-core-1.3.jar – Required to run JUnit tests
	Junit-4.12.jar - Required to run JUnit tests

Operation		
Execute from command line using the following command: 	

java -cp .:gson-2.2.4.jar:error.jar MvMain	

Enter a movie title in the test box field. The provided movie title will be used as a TheMovieDB.org query. Movie information will be displayed. 

No relevant results or invalid input will display an error message. 
