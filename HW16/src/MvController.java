
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class MvController {

	@FXML
	private ImageView imgPoster;

	@FXML
	private Label lblTitle;

	@FXML
	private Label lblRelease;

	@FXML
	private Label lblSummary;

	@FXML
	private Label lblErrSummary;
	
	@FXML
	private Label lblSummHead;
	
	@FXML
	private Label lblTitleHead;
	
	@FXML
	private Label lblReleaseHead;

	@FXML
	private Button btnSrch;
	
	@FXML
	private TextField txtQuery;

	@FXML
	private void handleButtonAction(ActionEvent e) {

		// Create object to access the Model
		String query = txtQuery.getText();
		MvModel m = new MvModel(query);
		
		// Has the go button been pressed?
		if (e.getSource() == btnSrch) {
			
			if (!m.isString(query)) {

				lblTitleHead.setText("");
				lblTitle.setText("");
				lblReleaseHead.setText("");
				lblRelease.setText("");
				lblSummHead.setText("");
				lblSummary.setText("");
				imgPoster.setImage(new Image("error.png"));
				lblErrSummary.setStyle("-fx-font-weight: bold;");
				lblErrSummary.setText("ERROR: Please use alphanumeric characters for your search. "
						+ "Do not start the search with a space or " + "special characters.");
				txtQuery.setText("Enter a movie title");
				txtQuery.requestFocus();
				txtQuery.selectAll();

			} else if (m.rsltCnt() <= 0) {

				lblTitleHead.setText("");
				lblTitle.setText("");
				lblReleaseHead.setText("");
				lblRelease.setText("");
				lblSummHead.setText("");
				lblSummary.setText("");
				imgPoster.setImage(new Image("error.png"));
				lblErrSummary.setStyle("-fx-font-weight: bold;");
				lblErrSummary.setText(
						"ERROR: No results found on TheMovieDB.org. " + "Please check the spelling of your movie title.");
				txtQuery.setText("Enter a movie title");
				txtQuery.requestFocus();
				txtQuery.selectAll();
			}

			else if (m.isNull("poster_path")) {

				lblTitleHead.setText("Title: ");
				lblTitle.setText(m.getTitle());
				lblReleaseHead.setText("Release: ");
				lblRelease.setText(m.getRlseDte());
				lblSummHead.setText("Summary: ");
				lblSummary.setText(m.getSumm());
				imgPoster.setImage(new Image("error.png"));
				lblErrSummary.setStyle("-fx-font-weight: bold;");
				lblErrSummary.setText("ERROR: No poster found.");
				txtQuery.setText("Enter a movie title");
				txtQuery.requestFocus();
				txtQuery.selectAll();
			}

			else {
				lblTitleHead.setText("Title: ");
				lblTitle.setText(m.getTitle());
				lblReleaseHead.setText("Release: ");
				lblRelease.setText(m.getRlseDte());
				lblSummHead.setText("Summary: ");
				lblSummary.setText(m.getSumm());
				imgPoster.setImage(m.getPoster());
				lblErrSummary.setText("");
				txtQuery.setText("Enter a movie title");
				txtQuery.requestFocus();
				txtQuery.selectAll();
			}
		}
	}
}
