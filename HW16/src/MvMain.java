import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MvMain extends Application {

	// Build JavaFX MvView
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("./MvView.fxml"));

		Scene scene = new Scene(root);

		stage.setScene(scene);
		stage.setTitle("Ben Alekseyev - CSCI 13 Final Project");
		stage.setResizable(false);
		stage.show();
	}

	// Launch JavaFX MvView
	public static void main(String[] args) {
		launch(args);
	}
}
