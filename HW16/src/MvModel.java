import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import com.google.gson.*;
import javafx.scene.image.Image;

public class MvModel {

	// Declare global objects
	private JsonElement jse;
	final static String API_KEY = "d559f2ae73bf012cf81f1aaad35c126d";

	// Constructor
	public MvModel(String in) {

		if (isString(in)) {

			final String URL = "https://api.themoviedb.org/3/search/movie?api_key=";
			try {

				// Construct TMDB.org API URL
				URL mvURL = new URL(URL + API_KEY + "&query=" + constrQry(in));

				// Open the URL
				InputStream is = mvURL.openStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(is));

				// Read the result into a JSON Element
				jse = new JsonParser().parse(br);

				// Close the connection
				is.close();
				br.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// Parse a multiple word input string to individual tokens
	// then reconstruct into a single '+' delimited string for URL
	public String constrQry(String in) {

		if (isString(in)) {

			String[] tokens = in.split(" +");
			int numTokens = tokens.length;

			String query = tokens[0];
			for (int i = 1; i < numTokens; i++) {

				query = query + "+" + tokens[i];
			}
			return query;
		}
		return null;
	}

	// Scrape movie title
	public String getTitle() {
		try {

			JsonObject jobject = jse.getAsJsonObject();
			JsonArray jarray = jobject.getAsJsonArray("results");
			JsonObject resultArr = jarray.get(0).getAsJsonObject();

			if (!isNull("title")) {

				String result = resultArr.get("title").getAsString();
				return result;
			} else {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// Scrape movie release date
	public String getRlseDte() {
		try {
			JsonObject jobject = jse.getAsJsonObject();
			JsonArray jarray = jobject.getAsJsonArray("results");
			JsonObject resultArr = jarray.get(0).getAsJsonObject();

			if (!isNull("release_date")) {

				String result = resultArr.get("release_date").getAsString();
				return result;
			} else {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// Scrape movie summary
	public String getSumm() {
		try {
			JsonObject jobject = jse.getAsJsonObject();
			JsonArray jarray = jobject.getAsJsonArray("results");
			JsonObject resultArr = jarray.get(0).getAsJsonObject();

			if (!isNull("overview")) {

				String result = resultArr.get("overview").getAsString();
				return result;
			} else {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	// Scrape movie poster
	public Image getPoster() {

		final String URL = "https://image.tmdb.org/t/p/w500";

		try {

			JsonObject jobject = jse.getAsJsonObject();
			JsonArray jarray = jobject.getAsJsonArray("results");
			JsonObject resultArr = jarray.get(0).getAsJsonObject();

			if (!isNull("poster_path")) {

				String result = resultArr.get("poster_path").getAsString();
				return new Image(URL + result);
			} else {

				return new Image("error.png");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new Image("error.png");
		}
	}

	// Use regex to check that input is alphanumeric and does not start with a
	// space
	public boolean isString(String in) {
		return (in.matches("\\w.*"));
	}

	public boolean isNull(String in) {
		JsonObject jobject = jse.getAsJsonObject();
		JsonArray jarray = jobject.getAsJsonArray("results");
		JsonObject resultArr = jarray.get(0).getAsJsonObject();
		boolean result = resultArr.get(in).isJsonNull();
		return result;
	}

	public int rsltCnt() {
		try {
			int num = jse.getAsJsonObject().get("total_results").getAsInt();

			return num;

		} catch (java.lang.NullPointerException npe) {
			return 0;
		}
	}
}