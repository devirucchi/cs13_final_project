import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MvModelTest {

	// themoviedb.org should return 1 result for "Better Luck Tomorrow"
	@Test
	public void testRsltCnt1() {

		MvModel m = new MvModel("Better Luck Tomorrow");
		assertEquals(1, m.rsltCnt());
	}

	// themoviedb.org should return 0 results for "Big Bang Theory"
	@Test
	public void testRsltCnt2() {

		MvModel m = new MvModel("Big Bang Theory");
		assertEquals(0, m.rsltCnt());
	}

	// themoviedb.org should return 1 result for "Better+Luck+Tomorrow"
	@Test
	public void testRsltCnt3() {

		MvModel m = new MvModel("Better+Luck+Tomorrow");
		assertEquals(1, m.rsltCnt());
	}

	// isString should not accept a space as a first character
	@Test
	public void testIsString1() {

		MvModel m = new MvModel(" ");
		assertEquals(false, m.isString(" "));
	}

	// isString should not accept non alphanumeric characters as a first
	// character
	@Test
	public void testIsString2() {

		MvModel m = new MvModel("!@#$%^&*()_+=~`- ");
		assertEquals(false, m.isString("!@#$%^&*()_+=~`- "));
	}

	// isString should accept non alphanumeric characters in the middle or end
	// of an alphanumeric query
	@Test
	public void testIsString3() {

		MvModel m = new MvModel("bEtter luck !@#$%^&*()_+= tomorrow");
		assertEquals(true, m.isString("bEtter luck !@#$%^&*()_+= tomorrow"));
	}

	// isString should accept non alphanumeric characters in the middle or end
	// of an alphanumeric query
	@Test
	public void testIsString4() {

		MvModel m = new MvModel("bEtter luck !@#$%^&*()_+=");
		assertEquals(true, m.isString("bEtter luck !@#$%^&*()_+="));
	}

	// test1 query "poster_path" value is null
	@Test
	public void testIsNull1() {

		MvModel m = new MvModel("test1");
		assertEquals(true, m.isNull("poster_path"));
	}

	// Wall-E query "poster_path" value is not null
	@Test
	public void testIsNull2() {

		MvModel m = new MvModel("Wall-E");
		assertEquals(false, m.isNull("poster_path"));
	}

	// Wall-E input should be reconstructed to Wall-E (no change)
	@Test
	public void testConstrQry1() {

		MvModel m = new MvModel("Wall-E");
		assertEquals("Wall-E", m.constrQry("Wall-E"));
	}

	// A white space query should return null
	@Test
	public void testConstrQry2() {

		MvModel m = new MvModel(" ");
		assertEquals(null, m.constrQry(" "));
	}

	// A non alphanumeric query should return null
	@Test
	public void testConstrQry3() {

		MvModel m = new MvModel("~!@#$%^&**()_");
		assertEquals(null, m.constrQry("~!@#$%^&**()_"));
	}

	// A two word title should return a single string with white space replaced
	// by a "+"
	@Test
	public void testConstrQry4() {

		MvModel m = new MvModel("John Wick");
		assertEquals("John+Wick", m.constrQry("John Wick"));
	}

	// A two word title with multiple white spaces should return a single string
	// with white space replaced by a "+"
	@Test
	public void testConstrQry5() {

		MvModel m = new MvModel("John          Wick");
		assertEquals("John+Wick", m.constrQry("John          Wick"));
	}

	// A three word title with multiple white spaces and accidental special
	// characters should return a single string
	// with white space replaced by a "+" and special characters mixed in
	@Test
	public void testConstrQry6() {

		MvModel m = new MvModel("Better     Luck *&%@tomorrow");
		assertEquals("Better+Luck+*&%@tomorrow", m.constrQry("Better     Luck *&%@tomorrow"));
	}

	// A poorly constructed query should still yield relevant results as long as
	// enough words match to an existing title
	@Test
	public void testGetTitle1() {

		MvModel m = new MvModel("Better     Luck *&%@tomorrow");
		assertEquals("Better Luck Tomorrow", m.getTitle());
	}
}
